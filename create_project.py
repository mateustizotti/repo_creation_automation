from requests import Session
import sys
import os

username = ''
password = ''
workspace = ''

session = Session()
session.auth = (username, password)

def main():
    repo_name = str(sys.argv[1])
    project = str(sys.argv[2])
    path = f'/Users/mtizotti/dev/{repo_name}'
    url = f'https://api.bitbucket.org/2.0/repositories/{workspace}/{repo_name}'
    os.makedirs(path)
    print(f'New folder created: {path}')

    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    data = {'scm': 'git', 'project': {'key': project}, 'is_private': True}
    r = session.post(url, headers=headers, json=data)
    if r.status_code == 200:
        print(f'Repository created on Bitbucket {repo_name}')
    else:
        print(f'Something went wrong creating the repository! - {r.text}')

if __name__ == '__main__':
    main()
    