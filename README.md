# Repository Autoamation with Python

---

### Setup:

1. Clone this repository locally;
2. Copy the files `.my_commands.sh` and `create_project.py` to your home directotry;
3. Edit both files in order to adjust your local paths and Bitbucket API credentials;
4. Edit your `.bashrc`/`.zshrc` and add the follwing line at the end:
```
source ~/.my_commands.sh
```
5. Relaunch your terminal;

### Usage:
From anywhere in your terminal, you can now type:
```
$ repo <repo_name> <BB_proj_key>
```
Where:

- `<repo_name>` will be the name of your new local folder and git repository on Bitbucket;
- `<BB_proj_key>` is the Key to the project you wish to add this repo to in Bitbucket;

### Results:
This script will:

1. Create a local folder in the specified path with your new repository name;
2. Create a Bitbucket repository with the same name;
3. Initialize it as a Git repository;
4. Add a `README.md` file;
5. Add the new origin URL (SSH format);
6. Make an initial commit;
7. Push it to the remote;
