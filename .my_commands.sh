#! /bin/bash

function repo() {
	cd
	python3 create_project.py $1 $2
	cd /Users/mtizotti/dev/$1
	git init
	git remote add origin git@bitbucket.org:mateustizotti/$1.git
	touch README.md
	git add .
	git commit -m "Initial commit"
	git push origin master
	code .
}